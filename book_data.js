//
// Hack for loading local files in Javascript: use a base-64 encoded version
// of the binary polyglot book and convert that back to binary format.
//

// var book_data = atob(`
// AAAAAAAAAABAUEdACjEuMAAAAAAAAAAACjIKMQpub3IAAAAAAAAAAG1hbApDcmVhAAAAAAAAAAB0
// ...
// 6ADsIOiMPwkuAAAAAAAA
// `);

//
// Global var: only one book loaded at any given time ;-)
//
// Start out with defined, but empty opening book
//
var book_data = '';
