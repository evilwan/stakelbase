# Stakelbase

Basically, Stakelbase is a viewer for Polyglot binary chess opening books. As such, it can be used as a training aid for studying
chess openings.

## Screenshot

![screenshot](images/screenshot.png)

## What does it do for me?

The tool lets you select any binary Polyglot chess opening book and then allows to browse
through the variations stored in that book. In each position, a list of registered continuations
from that position is shown. Click (select) one variation and the alternatives for the next move
are shown.

The list of variations is sorted in descending order of the weights in the opening book, so the
move with the highest score is shown first.

Stakelbase is intended to be used with personal opening repertoire files.

## Usage

Download all the files in this repository in the same local directory. Next open testsb.html in a web browser (with Javascript enabled) That's it.

## Quick introduction of the tool

After loading the interface, the first thing to do is to load a binary opening book from file (usually with extension .bin)
Such opening books can be downloaded from the Internet. Many tools (e.g. SCID) come with one or more
opening books in this format. Finally, it is possible (advisable?) to create new books with the polyglot command line tool.
That tool can be found on the web (a fork can be found here: https://github.com/evilwan/polyglot)

Once loaded, the interface will show a list of possible continuations: click on the variation of your choice. That move
will be performed on the chess board and the list of variations is refreshed with the know continuations from the new
board position. Repeat the process until no more continations are shown: you have reached the end of that variation in the book.

The arrow keys on the bottom of the screen can be used to go back and forth in the moves (e.g. to go back and select a different continuation)

The button with label "|<" returns to the start of the game (initial board position).

The button with label ">|" will advance in the current variation until either out of book, or when the book contains
more than one continuation.

The rotate board button does exactly that: flip the board.

## Creating your personal opening repertoire files

Stakelbase requires a binary opening book. There are several such books available on the Internet, but this tool is most useful when
you use your own, personal, tailored opening books. Fortunately, creating such custom files is rather straighforward and can easily be done
using free (as in beer) open source tools.

### Creating an opening repertoire

This step may be the most time consuming as it requires you to list all the variations that you want to be part of the training. For example,
you could start with a list of variations that you want to memorize against the Caro-Kann when playing white. How you obtain that list of variations
is up to you.

Some people suggest that you start from a large list of Caro-Kann games and drill down from there. For example, weed out all games that were played by
people with a rating less than 2400 (with the intention to keep only games between people who presumably know a thing or two about chess and therefore
eliminate more stupid mistakes that might have been played by less experienced players) Using chess database tools, it is possible to filter out such
games rather quickly. When the list of games has become manageable, the games can be exported to PGN format.

Another alternative could be to start from one or more books on the Caro-Kann where you select those variations that you want to incorporate in
your repertoire. This is my personal favorite. Usually I start by creating a new game in one of the Scid alternatives (e.g. [Scid](https://scid.sourceforge.net/)
or [Scid-vs-Pc](https://scidvspc.sourceforge.net/) ) but, of course, [Chessbase](https://en.chessbase.com/ ) can also be used. In that game, I enter all
the variations that I want to be part of my personal repertoire. When done, the single game is exported to PGN format.

### Creating a PGN file with one single game per variation

Before creating the personal book file from the PGN data, it is necessary to ensure that every variation is registered as a separate game in the PGN data.

Fortunately, this can easily be done using the open source tool [pgn-extract](https://www.cs.kent.ac.uk/~djb/pgn-extract):

```
pgn-extract --splitvariants myvariations.pgn > myvariations_split.pgn
```

### Creating the binary opening book for Stakelbase

The last step of the preparation requires the usage of `polyglot`. This tool can be found at [Github](https://github.com/ddugovic/polyglot)

However, I prefer to use my own [fork](https://codeberg.org/evilwan/polyglot) of this tool. The original Polyglot tool has some interesting logic
as to which variations to keep in the output file, based on game results. However, since I enter only those variations that I intend to play, I would rather
keep all variations that I entered. To that end, I added an additional option `-keepall` so that all variations from the PGN input will be written to the
output file. A sample usage:

```
polyglot make-book -pgn myvariations_split.pgn -bin mycaro-kann.bin -keepall
```

## Why HTML and Javascript?

This tool is intended to be used on as many platforms as possible, preferably requiring only a web browser (think Android devices and chromebooks)

As such, one of the design goals was to keep it simple and self-contained, so no external dependencies (no jquery, no node.js ...)

In the end, it is perfectly possible to merge all Javascript code in one big file, or maybe even include it as inline Javascript code
in the HTML page.

## Why this name?

The name is derived from the Dutch word "stekelbees", which translates to English as "gooseberry".

In Dutch, "stekels" means thorns or sharp branches, which looked appropriate for handling all the various
continuation lines that can arise from chess positions. The second part, "bees" is pronounced like the English
word "base", which seems applicable to everything chess database related.

## License

This tool is released under the BSD 3 clauses license: see LICENSE file.
